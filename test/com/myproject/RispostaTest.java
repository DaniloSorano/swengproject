package com.myproject;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Test;

import com.myproject.shared.Risposta;
import com.myproject.shared.Utente;
//
public class RispostaTest {
	
	Utente utente = new Utente("nomeUtente", "nome", "cognome", "dataNascita", "sesso", 
			"residenza", "luogoNascita", "email", "password");
	Risposta risposta = new Risposta(utente, 1, "testoDomanda");

	@Test
	public void testGetDataOra() {
		Date data=new Date();
		String d=data.toString();
		String[] t=d.split(" ");
		String dataOra =t[2]+" "+t[1]+" "+t[t.length-1]+" "+t[3];
		assertEquals(dataOra,risposta.getDataora());
	}
	
	@Test
	public void testGetId() {
		assertEquals(0,risposta.getId());
	}
	
	@Test
	public void testSetId() {
		risposta.setId(1);
		assertEquals(1,risposta.getId());
	}
	
	@Test
	public void testGetIdDomanda() {
		assertEquals(1,risposta.getIdDomanda());
	}
	
	@Test
	public void testSetIdDomanda() {
		risposta.setIdDomanda(2);
		assertEquals(2,risposta.getIdDomanda());
	}
	
	@Test
	public void testGetMedia() {
		assertEquals(-1,risposta.getMedia(),0);
	}
	
	@Test
	public void testSetMedia() {
		risposta.setMedia(2.553);
		assertEquals(2.553,risposta.getMedia(),0);
	}
	
	@Test
	public void testGetTesto() {
		assertEquals("testoDomanda",risposta.getTesto());
	}
	
	@Test
	public void testSetTesto() {
		risposta.setTesto("nuovoTestoDomanda");
		assertEquals("nuovoTestoDomanda",risposta.getTesto());
	}
	
	@Test
	public void testGetUtente() {
		assertEquals(utente,risposta.getUtente());
	}
	
	@Test
	public void testSetUtente() {
		Utente nuovoUtente = new Utente("nuovoNomeUtente", "nuovoNome", "nuovoCognome", "nuocoDataNascita", "nuovoSesso", 
				"nuovoResidenza", "nuovoLuogoNascita", "nuvoEmail", "nuovoPassword");
		risposta.setUtente(nuovoUtente);
		assertEquals(nuovoUtente,risposta.getUtente());
	}
}
