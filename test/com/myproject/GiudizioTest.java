package com.myproject;

import static org.junit.Assert.*;

import org.junit.Test;

import com.myproject.shared.Giudizio;
import com.myproject.shared.Utente;
// Testing della classe condivisa Giudizio
public class GiudizioTest {

	Utente utente = new Utente("nomeUtente", "nome", "cognome", "dataNascita", "sesso", 
			"residenza", "luogoNascita", "email", "password");
	Giudizio giudizio = new Giudizio(0,1,utente);
	// Testing del metodo getId
	@Test
	public void testGetId() {
		assertEquals(1,giudizio.getId());
	}
	//Testing del metodo setId
	@Test
	public void testSetId() {
		giudizio.setId(2);
		assertEquals(2,giudizio.getId());
	}
	// Testing del metodo getVoto
	@Test
	public void testGetVoto() {
		assertEquals(0,giudizio.getVoto());
	}
	// Testing del metodo setVoto
	@Test
	public void testSetVoto() {
		giudizio.setVoto(1);
		assertEquals(1,giudizio.getVoto());
	}
	// Testing del metodo getUtente
	@Test
	public void testGetUtente() {
		assertEquals(utente,giudizio.getUtente());
	}
	// Testing del metodo setUtente
	@Test
	public void testSetUtente() {
		Utente nuovoUtente = new Utente("nuovoNomeUtente", "nuovoNome", "nuovoCognome", "nuocoDataNascita", "nuovoSesso", 
				"nuovoResidenza", "nuovoLuogoNascita", "nuvoEmail", "nuovoPassword");
		giudizio.setUtente(nuovoUtente);
		assertEquals(nuovoUtente,giudizio.getUtente());
	}

}
