package com.myproject;

import static org.junit.Assert.*;

import org.junit.Test;

import com.myproject.shared.Categoria;
import com.myproject.shared.Domanda;
import com.myproject.shared.Utente;

/*
 * Testing della classe condivisa Domanda
 */
public class DomandaTest {
	// Inizializziamo gli oggeti utilizzati dai metodi
	Categoria cat = new Categoria("Nome Categoria");
	Utente utente = new Utente("nomeUtente", "nome", "cognome", "dataNascita", "sesso", 
			"residenza", "luogoNascita", "email", "password");
	Domanda domanda = new Domanda(utente, cat, "testo");
	// Testing del metodo getTesto
	@Test
	public void testGetTesto() {
		assertEquals("testo",domanda.getTesto());
	}
	//Testing del metodo setTesto
	@Test
	public void testSetTesto() {
		domanda.setTesto("nuovoTesto");
		assertEquals("nuovoTesto",domanda.getTesto());
	}
	//Testing del metodo getId
	@Test
	public void testGetId() {
		assertEquals(0,domanda.getId());
	}
	//Testing del metodo setId
	@Test
	public void testSetId() {
		domanda.setId(1);
		assertEquals(1,domanda.getId());
	}
	//Testing del metodo getCategoria
	@Test
	public void testGetCategoria() {
		assertEquals(cat,domanda.getCategoria());
	}
	//Testing del metodo setCategoria
	@Test
	public void testSetCategoria() {
		Categoria nuovaCategoria = new Categoria("nuovaCategoria");
		domanda.setCategoria(nuovaCategoria);
		assertEquals(nuovaCategoria,domanda.getCategoria());
	}
	//Testing del metodo getUtente
	@Test
	public void testGetUtente() {
		assertEquals(cat,domanda.getCategoria());
	}
	//Testing del metodo setUtente
	@Test
	public void testSetUtente() {
		Utente nuovoUtente = new Utente("nuovoNomeUtente", "nuovoNome", "nuovoCognome", "nuocoDataNascita", "nuovoSesso", 
				"nuovoResidenza", "nuovoLuogoNascita", "nuvoEmail", "nuovoPassword");
		domanda.setUtente(nuovoUtente);
		assertEquals(nuovoUtente,domanda.getUtente());
	}

}
