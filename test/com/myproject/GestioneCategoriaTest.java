package com.myproject;

import static org.junit.Assert.*;

import org.junit.Test;

import com.myproject.shared.Categoria;
import com.myproject.shared.GestioneCategoria;
// Testing classe GestioneCategoria
public class GestioneCategoriaTest {

	Categoria root=new Categoria("Categorie");
	Categoria figlio1=new Categoria("figlio1");
	Categoria figlio2=new Categoria("figlio2");
	Categoria figlio3=new Categoria("figlio3");
	Categoria figlio4=new Categoria("figlio4");
	//Testing del metodo trovaCategoria found case
	@Test
	public void testTrovaCategoriaFoundCase() {
		root.addSottoCategoria(figlio3);
		root.addSottoCategoria(figlio1);
		figlio1.addSottoCategoria(figlio2);
		assertEquals(1,GestioneCategoria.trovaCategoria(root,"figlio1"));
	}
	//Testing del metodo trovaCategoria not found case
	@Test
	public void testTrovaCategoriaNotFoundCase() {
		root.addSottoCategoria(figlio3);
		root.addSottoCategoria(figlio1);
		figlio1.addSottoCategoria(figlio2);
		assertEquals(0,GestioneCategoria.trovaCategoria(root,"figlio4"));
	}
	//Testing del metodo checkCategoria true case
	@Test
	public void testCheckCategoriaTrueCase() {
		root.addSottoCategoria(figlio3);
		root.addSottoCategoria(figlio1);
		figlio1.addSottoCategoria(figlio2);
		assertEquals(true, GestioneCategoria.checkCategoria(figlio2,figlio1));
	}
	//Testing del metodo checkCategoria false case
	@Test
	public void testCheckCategoriaFalseCase() {
		root.addSottoCategoria(figlio3);
		root.addSottoCategoria(figlio1);
		figlio1.addSottoCategoria(figlio2);
		assertEquals(false, GestioneCategoria.checkCategoria(figlio3,figlio1));
	}
	//Testing del metodo stampaCategorie
	@Test
	public void testStampaCategorie() {
		root.addSottoCategoria(figlio3);
		root.addSottoCategoria(figlio1);
		figlio1.addSottoCategoria(figlio2);
		assertEquals("figlio1 > figlio2",GestioneCategoria.stampaCategorie(figlio2, root));
	}
	//Testing del metodo renameCategoria
	@Test
	public void testRenameCategoria() {
		root.addSottoCategoria(figlio3);
		root.addSottoCategoria(figlio1);
		figlio1.addSottoCategoria(figlio2);
		GestioneCategoria.renameCategoriaHandler(root,"figlio1","newfiglio1");
		assertEquals("newfiglio1",figlio1.getNome());
	}
	//Testing del metodo trovaNodo
	@Test
	public void testTrovaNodo() {
		root.addSottoCategoria(figlio3);
		root.addSottoCategoria(figlio1);
		figlio1.addSottoCategoria(figlio2);
		assertEquals(figlio1,GestioneCategoria.trovaNodo(root,"figlio1"));
	}
}
