package com.myproject;

import static org.junit.Assert.*;

import org.junit.Test;

import com.myproject.shared.Utente;

public class UtenteTest {

	Utente utente = new Utente("nomeUtente", "nome", "cognome", "dataNascita", "sesso", 
			"residenza", "luogoNascita", "email", "password");

	@Test
	public void testGetNomeUtente() {
		assertEquals("nomeUtente",utente.getNomeUtente());
	}
	
	@Test
	public void testSetNomeUtente() {
		utente.setNomeUtente("nuovoNomeUtente");
		assertEquals("nuovoNomeUtente",utente.getNomeUtente());
	}
	
	@Test
	public void testGetNome() {
		assertEquals("nome",utente.getNome());
	}
	
	@Test
	public void testSetNome() {
		utente.setNome("nuovoNome");
		assertEquals("nuovoNome",utente.getNome());
	}
	
	@Test
	public void testGetCognome() {
		assertEquals("cognome",utente.getCognome());
	}
	
	@Test
	public void testSetCognome() {
		utente.setCognome("nuovoCognome");
		assertEquals("nuovoCognome",utente.getCognome());
	}
	
	@Test
	public void testGetDataNascita() {
		assertEquals("dataNascita",utente.getDataNascita());
	}
	
	@Test
	public void testSetDataNascita() {
		utente.setDataNascita("nuovaDataNascita");
		assertEquals("nuovaDataNascita",utente.getDataNascita());
	}
	
	@Test
	public void testGetSesso() {
		assertEquals("sesso",utente.getSesso());
	}
	
	@Test
	public void testSetSesso() {
		utente.setSesso("nuovoSesso");
		assertEquals("nuovoSesso",utente.getSesso());
	}
	
	@Test
	public void testGetResidenza() {
		assertEquals("residenza",utente.getResidenza());
	}
	
	@Test
	public void testSetResidenza() {
		utente.setResidenza("nuovaResidenza");
		assertEquals("nuovaResidenza",utente.getResidenza());
	}
	
	@Test
	public void testGetLuogoNascita() {
		assertEquals("luogoNascita",utente.getLuogoNascita());
	}
	
	@Test
	public void testSetLuogoNascita() {
		utente.setLuogoNascita("nuovoLuogoNascita");
		assertEquals("nuovoLuogoNascita",utente.getLuogoNascita());
	}
	
	@Test
	public void testGetEmail() {
		assertEquals("email",utente.getEmail());
	}
	
	@Test
	public void testSetEmail() {
		utente.setEmail("nuovaEmail");
		assertEquals("nuovaEmail",utente.getEmail());
	}
	
	@Test
	public void testGetPassword() {
		assertEquals("password",utente.getPassword());
	}
	
	@Test
	public void testSetPassword() {
		utente.setPassword("nuovaPassword");
		assertEquals("nuovaPassword",utente.getPassword());
	}

}
