package com.myproject;

import static org.junit.Assert.*;

import org.junit.Test;

import com.myproject.shared.Categoria;

/*
 * Testing della classe Categoria 
 */
public class CategoriaTest {
	//Inizializziamo gli oggetti utilizzati dai metodi di Testing
	Categoria categoria = new Categoria("root");
	Categoria padre=new Categoria("padre");
	Categoria figlio=new Categoria("figlio");
	
	// Testing del metodo getNome
	@Test
	public void testGetNome() {
		assertEquals("root",categoria.getNome());
	}
	// Testing del metodo setNome
	@Test
	public void testSetNome() {
		categoria.setNome("nuovaRoot");
		assertEquals("nuovaRoot",categoria.getNome());
	}
	// Testing dei metodi setPadre e getPadre
	@Test
	public void testSetGetPadre() {
		categoria.setPadre(padre);
		assertEquals(padre,categoria.getPadre());
	}
	// Testing dei metodi addSottoCategoria e getSottoCategoria
	@Test
	public void testAddGetSottoCategoria() {
		categoria.addSottoCategoria(figlio);
		assertEquals(figlio,categoria.getSottoCategoria().get(0));
	}
	// Testing del metodo removeSottoCategoria
	@Test
	public void testRemoveSottoCategoria() {
		categoria.addSottoCategoria(figlio);
		categoria.removeSottoCategoria("figlio");
		assertEquals(null,categoria.getSottoCategoria());
	}

}
