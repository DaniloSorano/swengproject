package com.myproject;

import static org.junit.Assert.*;

import org.junit.Test;

import com.myproject.shared.FieldVerifier;
// Testing della classe FieldVerifier
public class FieldVerifierTest {
	// Testing del metodo statico isValid caso true
	@Test
	public void testIsValidTrue() {
		assertEquals(true,FieldVerifier.isValid("AAAA"));
	}
	// Testing del metodo statico isValid caso false
	@Test
	public void testIsValidFalse() {
		assertEquals(false,FieldVerifier.isValid(null));
	}
	// Testing del metodo isValidField caso true
	@Test
	public void testIsValidFieldTrue() {
		assertEquals(true,FieldVerifier.isValidField("AAAAA"));
	}
	// Testing del metodo isValidField caso false
	@Test
	public void testIsValidFieldFalse() {
		assertEquals(false,FieldVerifier.isValidField("AAA"));
	}
	// Testing del metodo isValidPassword caso true
	@Test
	public void testIsValidPasswordTrue() {
		assertEquals(true,FieldVerifier.isValidPassword("pass", "pass"));
	}
	// Testing del metodo isValidPassword caso false
	@Test
	public void testIsValidPasswordFalse() {
		assertEquals(false,FieldVerifier.isValidPassword("pass", "AAA"));
	}
	// Testing del metodo passwordLength caso true
	@Test
	public void testPasswordLenghtTrue() {
		assertEquals(true,FieldVerifier.passwordLenght("passwordlenght"));
	}
	// Testing del metodo passwordLength caso false
	@Test
	public void testPasswordLenghtFalse() {
		assertEquals(false,FieldVerifier.passwordLenght("pass"));
	}

}
