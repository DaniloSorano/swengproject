package com.myproject.server;

import com.myproject.client.DbHandler;
import com.myproject.shared.Categoria;
import com.myproject.shared.Domanda;
import com.myproject.shared.GestioneCategoria;
import com.myproject.shared.Giudizio;
import com.myproject.shared.Risposta;
import com.myproject.shared.Utente;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentNavigableMap;

import javax.servlet.ServletContext;

import org.mapdb.DB;
import org.mapdb.DBMaker;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

/**
 * The server-side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class DbHandlerImpl extends RemoteServiceServlet implements DbHandler {

	public  synchronized String addUser(Utente userData) throws IllegalArgumentException {
		// Creo o mi connetto al db aaa
		DB database = getDB();
		// Creo l'oggetto map che contiene il nodo gi� esistente oppure lo crea
		ConcurrentNavigableMap<Integer, Utente> map = database.getTreeMap("Utenti");
		if(map.isEmpty()){
			Utente admin= new Utente("admin","admin");
			map.put(1,admin);
			database.commit();
		}
		ArrayList<Utente> users = new ArrayList<Utente>();
		Set<Integer> keys = map.keySet();
		//Metto gli elementi del nodo 'Utenti' all'interno di una lista di Utenti
		for(int key : keys) {
			users.add(map.get(key));
		}
		// Controllo se l'utente che vuole registrarsi esiste gi� oppure no
		for(int i = 0; i < users.size(); i++){
			if(userData.getNomeUtente().equals(users.get(i).getNomeUtente())){
				return "Failed";
			}
		}
		// Inserisco il nuovo Utente
		map.put(map.size() + 1, userData);
		database.commit();
		return "Success";
	}
	//Con questo metodo otteniamo come risultato le categorie su mapDB
	public synchronized Categoria getCategorie() {
		DB database = getDB(); //richiamiamo il DB
		ConcurrentNavigableMap<Integer, Categoria> map = database.getTreeMap("Categorie"); //istanzio un oggetto MAP su cui carichiamo le categorie
		if(database.getTreeMap("Categorie").isEmpty())
		{
			//se il nodo � vuoto, carico le categorie di base
			Categoria root = new Categoria("Categorie");
			Categoria ambiente = new Categoria("Ambiente");
			Categoria animali = new Categoria("Animali");
			Categoria arteCultura = new Categoria("Arte e Cultura");
			Categoria elettronicaTecnologia = new Categoria("Elettronica e Tecnologia");
			Categoria sport = new Categoria("Sport");
			Categoria svago = new Categoria("Svago");
			root.addSottoCategoria(ambiente);
			root.addSottoCategoria(animali);
			root.addSottoCategoria(arteCultura);
			root.addSottoCategoria(elettronicaTecnologia);
			root.addSottoCategoria(sport);
			root.addSottoCategoria(svago);
			map.put(1, root);
			database.commit();
		}
		return map.get(1);
		
	}
	//metodo per aggiungere una nuova categoria cliccando sulla categoria "padre"
	public synchronized boolean addCategoria(Categoria r){
		DB database = getDB();
		ConcurrentNavigableMap<Integer, Categoria> map = database.getTreeMap("Categorie");
		Categoria root=map.get(1); //richiamo il nodo ROOT contenente tutte le categorie e sotto categorie caricate in quel momento
		if(GestioneCategoria.trovaCategoria(root,r.getNome())==1){ //Se la categoria � root
			return false;
		}
		Categoria padre=GestioneCategoria.trovaNodo(root,r.getPadre().getNome()); //troviamo il padre della categoria
		if (padre==null){
			return false;
		}else{
			//aggiungiamo una categoria al padre cos� trovato
			padre.addSottoCategoria(r);
			map.remove(1);
			map.put(1,root);
			database.commit();
			return true;
		}	
	}
	
	
    //restituiamo il DB all'ultimo aggiornamento
	private DB getDB() {
		ServletContext context = this.getServletContext();
		synchronized (context) {
			DB db = (DB)context.getAttribute("DB");
			if(db == null) {
				db = DBMaker.newFileDB(new File("QuestionAnswerDb")).make();
				context.setAttribute("DB", db);
			}
			return db;
		}
	}

	//meotodo per il controllo dell'utente
	@Override
	public synchronized Utente checkUser(String[] userLogin) throws IllegalArgumentException {
		
		DB database = getDB();
		// Creo l'oggetto map che contiene il nodo gi� esistente oppure lo crea
		ConcurrentNavigableMap<Integer, Utente> map = database.getTreeMap("Utenti");
		if(map.isEmpty()){
			Utente admin= new Utente("admin","admin");
			map.put(1,admin);
			database.commit();
		}
		List<Utente> users = new ArrayList<Utente>();
		Set<Integer> keys = map.keySet();
		//Metto gli elementi del nodo 'Utenti' all'interno di una lista di Utenti
		for(int key : keys) {
			users.add(map.get(key));
		}
		// Controllo se l'utente che vuole accedere oppure no
		for(int i = 0; i < users.size(); i++){
			// Se esiste ritorno true
			if(userLogin[0].equals(users.get(i).getNomeUtente()) && 
					userLogin[1].equals(users.get(i).getPassword())) {
				return users.get(i);
			}
		}
		//Altrimenti false
		return null;
	}
	//metodo per la rimozione di una categoria
	@Override
	public synchronized boolean removeCategoria(String son){
		DB database = getDB();
		ConcurrentNavigableMap<Integer, Categoria> map = database.getTreeMap("Categorie");
		Categoria root=map.get(1);
		//int cate = GestioneCategoria.trovaCategoria(root,son);
		if(GestioneCategoria.removeCategoriaHandler(root,son)==1){ //se trovo il nodo da rimuovere
			map.remove(1);
			map.put(1,root);
			database.commit();
			return true;
		}else{//altrimenti false
			return false;
		}
	}
	//metodo per aggiungere una domanda cliccando sul bottone del panel che prendera l'oggetto domanda di riferimetno
	@Override
	public synchronized boolean addDomanda(Domanda dom) {
		DB database = getDB();
		ConcurrentNavigableMap<Integer, Domanda> map = database.getTreeMap("Domande"); //carico le domande su un oggetto map
		if(map.size() == 0){
			dom.setId(1);
			map.put(1, dom); //se l'oggetto � vuoto inserisco la domanda
		}
		else
		{ //altrimenti, tramite l'id inserisco la domanda in fondo alla lista assegnando il valore dell'ultima chiave+1
			dom.setId(map.lastKey()+1);
			map.put(map.lastKey() + 1, dom);
		}
		
		database.commit();
		return true;
	}
	//metodo per restituire domande presenti sul DB in quel momento
	@Override
	public synchronized ArrayList<Domanda> getDomanda() {
		DB database = getDB();
		ConcurrentNavigableMap<Integer, Domanda> map = database.getTreeMap("Domande");
		ArrayList<Domanda> domande = new ArrayList<Domanda>();
		Set<Integer> keys = map.keySet();
		for(int key : keys) { //eseguo un for che mi percorra tutto l'oggetto di tipo MAP in base alla chiave
			domande.add(map.get(key));
		}
		return domande;
	}
	//metood simile a quello precedente, ma in questo caso restituisce le domande filtrate in base alla categoria cliccata
	@Override
	public synchronized ArrayList<Domanda> getDomandaFiltro(String categoria) {
		DB database = getDB();
		ConcurrentNavigableMap<Integer, Categoria> map = database.getTreeMap("Categorie");
		ConcurrentNavigableMap<Integer, Domanda> dom = database.getTreeMap("Domande");
		ArrayList<Domanda> domande =new ArrayList<Domanda>();
		Set<Integer> keys = dom.keySet();
		for(int key : keys) {
			domande.add(dom.get(key));
		}
		if(categoria.equals("Categorie"))
		{
			return domande;
		}
		else
		{// filtro vero e proprio
			ArrayList<Domanda> risultato=new ArrayList<Domanda>();
			Categoria cat=GestioneCategoria.trovaNodo(map.get(1),categoria);
			for(int i=0;i<domande.size();i++){
				if(GestioneCategoria.checkCategoria(domande.get(i).getCategoria(),cat)){
					risultato.add(domande.get(i));
				}
			}
			return risultato;
		}
	}
	//metodo per la rimozione delle domande in base alla domanda cliccata
	@Override
	public synchronized boolean deleteDomanda(int dom) {
		DB database = getDB();
		ConcurrentNavigableMap<Integer, Domanda> mapDomande = database.getTreeMap("Domande");
		mapDomande.remove(dom); //rimuovo la domanda
		ConcurrentNavigableMap<Integer, Risposta> mapRisposte = database.getTreeMap("Risposte");
		Set<Integer> keysRisposte = mapRisposte.keySet();
		ConcurrentNavigableMap<Integer, Giudizio> mapGiudizi = database.getTreeMap("Giudizi");
		Set<Integer> keysGiudizi = mapGiudizi.keySet();
		for(int keyR : keysRisposte) { //prima eseguiamo un for per ottenere tutte le risposte della domanda
			if(mapRisposte.get(keyR).getIdDomanda() == dom)
			{
				int idRisposta = mapRisposte.get(keyR).getId();
				for(int keyG : keysGiudizi) { //per ogni risposta elimino i giudizi
					if(mapGiudizi.get(keyG).getId() == idRisposta)
					{
						mapGiudizi.remove(keyG);
					}
				}
				mapRisposte.remove(keyR); //elimino tutte risposte della domanda
			}
		}
		database.commit();
		
		return true;
	}
	//aggiungo una risposta
	@Override
	public synchronized boolean addRisposta(Risposta risp) {
		DB database = getDB();
		ConcurrentNavigableMap<Integer, Risposta> map = database.getTreeMap("Risposte");
		if(map.size() == 0){ //se non ci sono risposte l'aggiungo
			risp.setId(1);
			map.put(1, risp);
		}
		else
		{//altrimenti viene aggiunta in fondo alla lista
			risp.setId(map.lastKey() + 1);
			map.put(map.lastKey()+ 1, risp);
		}
		
		database.commit();
		return true;
	}
	//ottengo le risposte filtrate in base alla domanda cliccata (o meglio in base all'id della domanda)
	@Override
	public ArrayList<Risposta> getRisposteFiltro(int domanda) {
		DB database = getDB();
		ConcurrentNavigableMap<Integer, Risposta> dom = database.getTreeMap("Risposte");
		ArrayList<Risposta> risposte =new ArrayList<Risposta>();
		Set<Integer> keys = dom.keySet();
		for(int key : keys) {
			if(dom.get(key).getIdDomanda() == domanda)
				risposte.add(dom.get(key));
		}

		return ordinaRisposte(risposte);		
	}
	//ordinamento delle risposte in modo cronologico
	private ArrayList <Risposta> ordinaCrono(ArrayList <Risposta> listaRisp){
		ArrayList<Risposta> risultato=new ArrayList<Risposta>();
		for(int i=listaRisp.size()-1;i>=0;i--){
			risultato.add(listaRisp.get(i));
		}
		return risultato;
	}
	private ArrayList <Risposta> ordinaRisposte(ArrayList <Risposta> risposte){
		ArrayList<Risposta> listaRisp=ordinaCrono(risposte);
		for(int i=0;i<listaRisp.size();i++){
			for(int j=0;j<listaRisp.size()-1;j++){
				if(listaRisp.get(j).getMedia()<listaRisp.get(j+1).getMedia()){
					Risposta k=listaRisp.get(j);
					listaRisp.set(j,listaRisp.get(j+1));
					listaRisp.set(j+1,k);
				}
			}
		}
		return listaRisp;
	}
	//rinomino una categoria
	@Override
	public synchronized boolean renameCategoria(String selezionata, String nuovoNome) {
		
		DB database = getDB();
		ConcurrentNavigableMap<Integer, Categoria> map = database.getTreeMap("Categorie");
		Categoria root=map.get(1);
		//int cate = GestioneCategoria.trovaCategoria(root,son);
		if(GestioneCategoria.renameCategoriaHandler(root, selezionata, nuovoNome)==1){//questo metodo prende in entrata la categoria e il nuovo nome
			map.remove(1);//prima rimuovo la cateogoria
			map.put(1,root);//aggiungo la categoria rinominata
			database.commit();
			return true;
		}else{
			return false;
		}
	}
	//questo metodo serve all'amministratore per ottenere una lista degli utenti semplici da nominare giudici
	@Override
	public ArrayList<Utente> getUtentiSemplici() {
		DB database = getDB();
		ConcurrentNavigableMap<Integer, Utente> map = database.getTreeMap("Utenti");
		ArrayList<Utente> users = new ArrayList<Utente>();
		Set<Integer> keys = map.keySet();
		//Metto gli elementi del nodo 'Utenti' all'interno di una lista di Utenti
		for(int key : keys) {
			if(map.get(key).getTipo().equals("Semplice")){
				users.add(map.get(key));
			}
		}
		return users;
	}
	//metodo per la nomina del giudice
	@Override
	public synchronized boolean nominaGiudice(String Utente) {
		DB database = getDB();
		// Creo l'oggetto map che contiene il nodo gi� esistente oppure lo crea
		ConcurrentNavigableMap<Integer, Utente> map = database.getTreeMap("Utenti");
		ArrayList<Utente> users = new ArrayList<Utente>();
		Set<Integer> keys = map.keySet();
		//Metto gli elementi del nodo 'Utenti' all'interno di una lista di Utenti
		for(int key : keys) {
			users.add(map.get(key));
		}
		//tramite un for ricerchiamo nella lista degli utenti, quello corrispondente al parametro di ingresso del metodo
		for(int i=0; i<users.size();i++){
			if(users.get(i).getNomeUtente().equals(Utente)){
				users.get(i).setTipoGiudice(); //settiamo come giudice 
				//users.remove(i);		
			}
		}
		for(int key : keys){
			map.remove(key);
		}
		for(int i=0; i<users.size();i++){
			map.put(i+1,users.get(i)); //elimino l'utente e lo reinserisco col nuovo tipo settato nel DB
		}
		database.commit();
		return true;
	}
	//metodo per aggiungere un giudizio alla risposta
	@Override
	public synchronized boolean addGiudizio(Giudizio giudizio) {
		DB database = getDB();
		// Creo l'oggetto map che contiene il nodo gi� esistente oppure lo crea
		ConcurrentNavigableMap<Integer, Giudizio> map = database.getTreeMap("Giudizi");
		if(map.size()==0){
			map.put(1,giudizio);	}else{
			map.put(map.lastKey()+1, giudizio);
		}
		ArrayList<Giudizio> listaGiudizi = new ArrayList<Giudizio>();
		Set<Integer> keys = map.keySet();
		//Metto gli elementi del nodo 'Utenti' all'interno di una lista di Utenti
		for(int key : keys) {
			listaGiudizi.add(map.get(key));
		}
		ConcurrentNavigableMap<Integer, Risposta> mapRisp = database.getTreeMap("Risposte");
		ArrayList<Risposta> listaRisposte = new ArrayList<Risposta>();
		Set<Integer> keysRisp = mapRisp.keySet();
		//Metto gli elementi del nodo 'Utenti' all'interno di una lista di Utenti
		for(int key : keysRisp) {
			if(key == giudizio.getId())
			{
				Risposta rispTemp = mapRisp.get(key);
				mapRisp.remove(key);
				rispTemp.setMedia(mediaGiudizi(giudizio.getId(), listaGiudizi));
				mapRisp.put(key, rispTemp);
			}
			listaRisposte.add(mapRisp.get(key));
		}
		
		database.commit();
		return true;
	}

	@Override
	public synchronized String pathCategorie(String categoria) {
		
		DB database = getDB();
		// Creo l'oggetto map che contiene il nodo gi� esistente oppure lo crea
		ConcurrentNavigableMap<Integer, Categoria> map = database.getTreeMap("Categorie");
		Categoria root = map.get(1);	
		Categoria cat = GestioneCategoria.trovaNodo(root, categoria); 
		return GestioneCategoria.stampaCategorie(cat, root);//otteniamo il percorso della categoria che vogliamo
		
	}

	@Override
	public synchronized ArrayList<Giudizio> getGiudizi(int idRisposta) {
		DB database = getDB();
		// Creo l'oggetto map che contiene il nodo gi� esistente oppure lo crea
		ConcurrentNavigableMap<Integer, Giudizio> map = database.getTreeMap("Giudizi");
		ArrayList<Giudizio> giudizi = new ArrayList<Giudizio>();
		Set<Integer> keys = map.keySet();
		for(int key : keys) {
			if(map.get(key).getId() == idRisposta) //otteniamo i giudizi in base all'id della risposta
			{
				giudizi.add(map.get(key));
			}
		}
		return giudizi;
	}
	//metodo per una media dei giudizi
	private double mediaGiudizi(int idRisposta,ArrayList<Giudizio> listaGiud){
		double m=0;
		int j=0;
		for(int i=0;i<listaGiud.size();i++){
			if(idRisposta == listaGiud.get(i).getId()){
				m=m+listaGiud.get(i).getVoto();
				j++;
			}
		}
		if(j!=0){
			m=m/j;
		}
		return m;
	}
	//metodo per giudicare una risposta
	@Override
	public synchronized boolean checkGiudizio(int idRisposta, Utente utente) {
		
		if(utente.getTipo().equals("Giudice"))
		{
			DB database = getDB();
			// Creo l'oggetto map che contiene il nodo gi� esistente oppure lo crea
			ConcurrentNavigableMap<Integer, Giudizio> map = database.getTreeMap("Giudizi");
			Set<Integer> keys = map.keySet();
			for(int key : keys) {
				if(map.get(key).getId() == idRisposta && map.get(key).getUtente().getNomeUtente().equals(utente.getNomeUtente()))
				{
					return true;
				}
			}
			return false;
		}
		else
		{
			return false;
		}
	}
	//metodo per eliminare una risposta
	@Override
	public synchronized boolean eliminaRisposta(int risp) {
		DB database = getDB();
		ConcurrentNavigableMap<Integer, Risposta> map = database.getTreeMap("Risposte");
		map.remove(risp);
		ConcurrentNavigableMap<Integer, Giudizio> mapGiudizi = database.getTreeMap("Giudizi"); //una volta eliminata la risposta eliminiamo anche tutti i giudizi relativi
		Set<Integer> keys = mapGiudizi.keySet();
		for(int key : keys) {
			if(mapGiudizi.get(key).getId() == risp)
			{
				mapGiudizi.remove(key);
			}
		}
		database.commit();
		return true;
	}	

	

}

