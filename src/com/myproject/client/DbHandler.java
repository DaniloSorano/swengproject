package com.myproject.client;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.myproject.shared.Categoria;
import com.myproject.shared.Domanda;
import com.myproject.shared.Giudizio;
import com.myproject.shared.Risposta;
import com.myproject.shared.Utente;

/*
 * Interfaccia sincrona del Db Handler
 * Qui venono dichiarati i metodi utili implementati sul server 
 * attraverso la classe DbHandlerImpl.java
*/
@RemoteServiceRelativePath("database")
public interface DbHandler extends RemoteService {
	
	// addUser permette di aggiungere un Utente
	String addUser(Utente userData) throws IllegalArgumentException;
	
	// checkUser controlla se un utente � gia registrato al sito
	Utente checkUser(String[] userLogin) throws IllegalArgumentException;
	
	//getCategorie ritorna la Categoria root con tutte le categorie e sotto categorie asscoiate
	Categoria getCategorie();
	
	// getDomanda ritorna tutte le domande del sito
	ArrayList<Domanda> getDomanda();
	
	// getDomandaFiltro ritorna tutte le domande per una data categoria
	ArrayList<Domanda> getDomandaFiltro(String categoria);
	
	// getRisposteFiltro ritorna tutte le risposte relative ad una domanda passando in input l'id della domanda
	ArrayList<Risposta> getRisposteFiltro(int domanda);

	// addDomanda aggiunge una domanda passando in input una oggeto domanda
	boolean addDomanda(Domanda dom);

	// addCategoria aggiunge una Categoria o una sottoCategoria
	boolean addCategoria(Categoria son);
	
	//removeCategoria rimuove la categoria Passata in input come parametro
	boolean removeCategoria(String son);

	// deleteDomanda elimina una domanda in base all'id passato in input
	boolean deleteDomanda(int dom);
	
	// renameCateogoria rinomina una categoria passando in input il nome della categoria 
	// da rinominare e il nuovo nome da assegnare
	boolean renameCategoria(String selezionata, String NuovoNome);

	// addRisposta aggiunge una risposta 
	boolean addRisposta(Risposta risp);
	
	// getUtentiSemplici ritorna tutti gli utenti semplici della piattaforma
	ArrayList<Utente> getUtentiSemplici();
	
	// il metodo nominaGiudice permette di nominare un utente giudice passando in input il nomeUtente
	boolean nominaGiudice(String Utente);
	
	// addGiudizio aggunge un giudizio di un utente
	boolean addGiudizio(Giudizio giudizio);
	
	// eliminaRisposta elimina la ripsota di un utente passando in input l'id della risposta
	boolean eliminaRisposta(int risp);
	
	// pathCategoria ritorna una stringa con il path della categoria passata in input a partire dalla root
	String pathCategorie(String categoria);
	
	// getGiudizi ritorna tutti i giudizi relativi ad una risposta
	ArrayList<Giudizio> getGiudizi(int idRisposta);
	
	// checkGiudizio controlla se un certo utente ha gi� giudicato una risposta
	boolean checkGiudizio (int idRisposta,Utente utente);
	
	
}
