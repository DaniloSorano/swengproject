package com.myproject.client;

import java.util.ArrayList;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Tree;
import com.google.gwt.user.client.ui.TreeItem;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.myproject.shared.Categoria;
import com.myproject.shared.GestioneCategoria;
import com.myproject.shared.Utente;

public class PanelCategorie {
	
	private String tipo;
	private PanelDomande domandePan;
	private DbHandlerAsync dbHandler;
	private Categoria root;
	private Utente utente;
	private Tree tree;
	private VerticalPanel vertPan;
	private TextBox nomeCategoria;
	private Button tastoInvio;
	private Button elimina;
	private Button rinomina;
	private Label labelCategoria;
	boolean invioIsVisible = false;
	
	// Il costruttore della classe panelCategorie inizialliza 
	// le variabili di istanza che utilizzera nei suoi metodi
	public PanelCategorie(Utente utente, DbHandlerAsync dbHandler, Tree tree) {
		// Se l'utente � un ospite l'oggeto relativo sar� di tipo null
		// pertanto assegniamo alla variabile di istanza il tipo 'Guest'
		if(utente != null)
		{
			tipo = utente.getTipo();
		}
		else
		{
			tipo = "Guest";
		}
		this.tree = tree;
		this.utente = utente;
		this.dbHandler = dbHandler;
	}
	// Questo metodo inizializza e aggiunge al RootPanel i widget 
	public void loadPanelCategorie() {
		
		vertPan = new VerticalPanel();
		vertPan.add(tree);
		// Se l'utente � Amministratore allora aggiungiamo i tasti rinomina ed elimina
		if(tipo.equals("Amministratore"))
		{
			elimina = new Button("Elimina");
			elimina.setStyleName("btn btn-danger");
			elimina.setVisible(false);
			rinomina = new Button("Rinomina");
			rinomina.setVisible(false);
			rinomina.setStyleName("btn btn-warning");
			nomeCategoria = new TextBox();
			nomeCategoria.setStyleName("form-control text-center");
			labelCategoria = new Label();
			labelCategoria.setStyleName("label label-default");
		}
		// Facciamo una chiamata asincrona al server utilizzando il metodo getCategorie
		// che ritornera un oggeto Categoria
		dbHandler.getCategorie(new AsyncCallback<Categoria>() {
			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Problemi con il server!!!");
			}
	
			@Override
			public void onSuccess(Categoria result) {
				initCategorie(result);
			}
		});
		// Ci occupiamo di gestire gli eventi dei tasti elimina e rinomina 
		// solo se l'utente � Amministratore
		if(tipo.equals("Amministratore"))
		{
			elimina.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				if(tree.getSelectedItem() == null || tree.getSelectedItem().getText().equals("Categorie"))
					Window.alert("Seleziona una categoria da eliminare");
				else
					dbHandler.removeCategoria(tree.getSelectedItem().getText(),new AsyncCallback<Boolean>(){
	
						@Override
						public void onFailure(Throwable caught) {
							Window.alert("Problemi con il Server!!!");
						}
	
						@Override
						public void onSuccess(Boolean result) {
							if(result){
								dbHandler.getCategorie(new AsyncCallback<Categoria>(){
	
									@Override
									public void onFailure(Throwable caught) {
										Window.alert("Problemi con il Server!!!");
									}
	
									@Override
									public void onSuccess(Categoria result) {
										if(result != null)
										{
											initCategorie(result);
										}
										else
										{
											Window.alert("Nessuna Categoria Presente!");
										}
									}
									
								});
							}
						}
						
					});
				}
			
			});
			rinomina.addClickHandler( new ClickHandler() {

				@Override
				public void onClick(ClickEvent event) {
					
					if(tree.getSelectedItem() == null || tree.getSelectedItem().getText().equals("Categorie")){
						Window.alert("Seleziona una categoria da rinominare");
					}else{
						
						vertPan.remove(tastoInvio);
						
						tastoInvio = new Button("Invio");
						
						vertPan.add(tastoInvio);
						invioIsVisible = true;
						
						labelCategoria.setText("Nuovo nome da assegnare alla Categoria");
						labelCategoria.setVisible(true);
						nomeCategoria.setText("");
						nomeCategoria.setVisible(true);
						
						final String selezionata = tree.getSelectedItem().getText();
						
						tastoInvio.addClickHandler(new ClickHandler(){
						
							@Override
							public void onClick(ClickEvent event) {
								
							dbHandler.renameCategoria(selezionata, nomeCategoria.getText(), new AsyncCallback<Boolean>(){

								@Override
								public void onFailure(Throwable caught) {
									Window.alert("Problemi con il Server!");
								}

								@Override
								public void onSuccess(Boolean result) {
									dbHandler.getCategorie(new AsyncCallback<Categoria>(){

										@Override
										public void onFailure(Throwable caught) {
											Window.alert("Problemi con il Server!");
										}

										@Override
										public void onSuccess(Categoria result) {
											if(result != null)
											{
												initCategorie(result);
											}
											else
											{
												Window.alert("Nessuna Categoria presente!");
											}
										}
										
									});
									
								}
								
								
							});
								
							}
						});
					}
				}
			});
		}
		//Infine inizializziamo l'oggeto panelDomande lanciando il suo metodo loadPanelDomande
		domandePan = new PanelDomande(utente, dbHandler);
		domandePan.loadPanelDomande();
		// il metodo privato treeSelectionHandler si occupa di gestire 
		// gli eventi di selezione del Tree
		treeSelectionHandler();
	}
	
	/*
	 *  Questo metodo privato si occupa di gestire l'evento di click degli elemeti del Tree
	*/
	private void treeSelectionHandler() {
		tree.addSelectionHandler(new SelectionHandler<TreeItem>()
		{
			@Override
			public void onSelection(SelectionEvent<TreeItem> event) {
				/*
				 * Quando un elemento viene cliccato ci si assicura che non si tratti 
				 * di una label per aggiungere gli elementi dopodiche ci si occupa di cambiare 
				 * la variabile di istanza categoriaSelected del tree attraverso il metodo setCategoriaSelected
				 */
				if(!event.getSelectedItem().getText().contains("Aggiungi nuova categoria a "))
				{
					domandePan.setCategoriaSelected(event.getSelectedItem().getText());
				}
				if(!event.getSelectedItem().getText().equals("Categorie") && !event.getSelectedItem().getText().contains("Aggiungi nuova categoria a "))
				{
					// Se clicchiamo una delle categorie vengono anche visualizzati i tasti rinomina e elimina
					if(tipo != null) {
						if(tipo.equals("Amministratore"))
						{
							if(invioIsVisible)
							{
								vertPan.remove(tastoInvio);
								invioIsVisible = false;
							}
							labelCategoria.setVisible(false);
							nomeCategoria.setVisible(false);
							elimina.setVisible(true);
							rinomina.setVisible(true);
						}
					}
				}
				else
				{
					if(tipo != null) {
						if(tipo.equals("Amministratore"))
						{
							if(invioIsVisible)
							{
								vertPan.remove(tastoInvio);
								invioIsVisible = false;
							}
							labelCategoria.setVisible(false);
							nomeCategoria.setVisible(false);
							elimina.setVisible(false);
							rinomina.setVisible(false);
						}
					}
				}
				
			}
		});
	}
	// Questo metodo privato prende in input la Categoria, essendo ricorsivo
	// si occupa di creare un oggetto treeItem che viene poi passato al oggetto
	// Tree
	private TreeItem createTreeItem(Categoria categoriaRoot) {
		// Per prima cosa ci assicuriamo che il nostro oggetto categoria non sia vuoto
		if(categoriaRoot != null)
		{
			Categoria temp;
			int trovato,i;
			trovato=0;
			i=0;
			ArrayList<Categoria> sottoCat=categoriaRoot.getSottoCategoria();
			// Se la categoria contiene sottocategoria quindi sottoCat non � nullo
			// scorriamo il suo arraylist
			if(sottoCat!=null){
				temp=sottoCat.get(i);
				// creiamo il primo oggeto TreeItem con il nome della categoria passata in input
				TreeItem treeItem = new TreeItem();
				treeItem.setText(categoriaRoot.getNome());
				//Scorriamo l'ArrayList di categorie
				while(i<sottoCat.size() && trovato!=1){
					//Aggiungiamo al nostro treeItem il risultato della chiamat ricorsiva che prende
					// input la categoria dell'arraylist
					// Cosi facendo otteniamo anche le categorie e sotto categorie associate a quella categoria
					treeItem.addItem(createTreeItem(temp));
					i++;
					// Arrivati alla fine dell'ArrayList aggiungiamo una label 
					// che quando viene cliccata permette di aggiungere una nuova categoria o sottocategoria
					if(i>=sottoCat.size())
					{
						if(tipo.equals("Amministratore")){
							Label lab =new Label("Aggiungi nuova categoria a " + categoriaRoot.getNome());
							treeItem.addItem(lab);
							final Categoria category = categoriaRoot;
							lab.setStyleName("label label-primary");
							lab.addClickHandler(new ClickHandler() {
								@Override
								public void onClick(ClickEvent event) {
									if(nomeCategoria.isVisible())
									{ 
										labelCategoria.setVisible(false);
										nomeCategoria.setVisible(false);
										vertPan.remove(tastoInvio);
									}
									else {
										labelCategoria.setText("Nome Categoria da Aggiungere a " + category.getNome());
										labelCategoria.setVisible(true);
										nomeCategoria.setText("");
										nomeCategoria.setVisible(true);
										Categoria figlio = new Categoria("default");
										figlio.setPadre(category);
										//Creiamo un nuova categoria di base il cui padre � l'oggeto categoriaRoot
										addCategoriaButtonHandler(figlio);
									}
								}	
							});
						}
						return treeItem;
					}
					temp=sottoCat.get(i);
				}
			}
			else
			{
				/*
				 * Se la categoria non ha sottoCategorie allora aggiungiamo 
				 * come sottocategoria la label per aggiungere le categorie
				 */
				TreeItem treeItem = new TreeItem();
				treeItem.setText(categoriaRoot.getNome());
				if(tipo.equals("Amministratore")){
					Label lab =new Label("Aggiungi nuova categoria a " + categoriaRoot.getNome());
					treeItem.addItem(lab);
					final Categoria category = categoriaRoot;
					lab.setStyleName("label label-success");
					lab.addClickHandler(new ClickHandler() {
						@Override
						public void onClick(ClickEvent event) {
						
							if(nomeCategoria.isVisible())
							{
								labelCategoria.setVisible(false);
								nomeCategoria.setVisible(false);
								vertPan.remove(tastoInvio);
							}
							else {
								labelCategoria.setText("Nome Categoria da Aggiungere a " + category.getNome());
								nomeCategoria.setText("");
								labelCategoria.setVisible(true);
								nomeCategoria.setVisible(true);
								Categoria figlio = new Categoria("default");
								//vertPan.remove(tastoInvio);
								figlio.setPadre(category);
								//Window.alert(category.getNome());
								addCategoriaButtonHandler(figlio);
							}
						}	
					});
				}
				return treeItem;	
				
			}
		}
		return null;
	}
	
	/*
	 * Questo metodo si occupa di creare il panel delle categorie, 
	 * quindi ad ogni operazione di aggiornamento questo metodo viene chiamato 
	 * al fine di visualizzare gli aggiornamenti
	 */
	private void initCategorie(Categoria result) {
		root=result;
		//Window.alert(root.getSottoCategoria().get(1).getNome());
		vertPan.clear();
		TreeItem rootTree = createTreeItem(root);
		tree.clear();
		tree.addItem(rootTree);
		vertPan.add(tree);
		if(tipo.equals("Amministratore"))
		{
			vertPan.add(elimina);
			vertPan.add(rinomina);
			elimina.setVisible(false);
			rinomina.setVisible(false);
			vertPan.add(labelCategoria);
			vertPan.add(nomeCategoria);
			nomeCategoria.setVisible(false);
			labelCategoria.setVisible(false);
			//vertPan.add(tastoInvio);
			//tastoInvio.setVisible(false);
		}
		RootPanel.get("leftPanel").add(vertPan);
	}
	
	// Questo metodo gestisce l'evento di click dell'aggiunta della categoria
	private void addCategoriaButtonHandler(Categoria categoria) {

		final Categoria figlio = categoria;
		figlio.setNome(nomeCategoria.getText());
		tastoInvio = new Button("Invio");
		vertPan.add(tastoInvio);
		invioIsVisible = true;
		tastoInvio.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				figlio.setNome(nomeCategoria.getText());
				if(!nomeCategoria.getText().isEmpty()){
				// dopo aver fatto gli adeguati controlli viene fatta una chiamata al server
				// con il metodo addCategoria
				dbHandler.addCategoria(figlio,new AsyncCallback<Boolean>(){
					@Override
					public void onFailure(Throwable caught) {
						Window.alert("Problemi con il Server!!!");
					}

					@Override
					public void onSuccess(Boolean result) {
						
						if(result == true)
						{
							dbHandler.getCategorie(new AsyncCallback<Categoria>() {
								@Override
								public void onFailure(Throwable caught) {
									Window.alert("Problemi con il server!!!");
								}

								@Override
								public void onSuccess(Categoria result) {
									initCategorie(result);
								}
							});
						}
						
					}			
				});
				}
				else
				{
					Window.alert("Attenzione!!! Inserisci il nome  della categoria");
				}
			}		
		});
	}
}
