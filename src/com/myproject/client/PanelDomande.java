package com.myproject.client;

import java.util.ArrayList;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.myproject.shared.Categoria;
import com.myproject.shared.Domanda;
import com.myproject.shared.GestioneCategoria;
import com.myproject.shared.Utente;

public class PanelDomande {
	private String tipo;
	private String categoriaSelected;
	private PanelRisposte rispostePan;
	private DbHandlerAsync dbHandler; 
	private Utente utente;
	private VerticalPanel questionPanel;
	private VerticalPanel formPanel;
	private Button insertDomanda;
	private TextArea testoDomanda;
	
	/*
	 * Il costruttore inizializza le variabili di istanza principali
	 */
	public PanelDomande(Utente utente, DbHandlerAsync dbHandler) {
		this.dbHandler = dbHandler;
		this.utente = utente;
		categoriaSelected = "Categorie";
		// Se l'oggetto utente � null la variabile tipo viene settata con la Stringa "Guest"
		if(utente == null){
			tipo = "Guest";
		}
		else
		{
			tipo = utente.getTipo();
		}	
	}
	/*
	 * Questo metodo si occupa di inizializzare, aggiungere al RootPanel 
	 * e gestire gli eventi di click dei vari Widget
	 */
	public void loadPanelDomande() {
		formPanel = new VerticalPanel();
		questionPanel=new VerticalPanel();
		RootPanel.get("insidePanel").add(questionPanel);
		// Per prima cosa viene fatta una chiamata al server attraverso il metodo getDomanda
		dbHandler.getDomanda(new AsyncCallback<ArrayList<Domanda>>(){
			
			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Problemi con il server!!!");
			}

			@Override
			public void onSuccess(ArrayList<Domanda> result) {
				initListaDomande(result);	
			}
		});
		// Si crea un oggetto PanelRisposte e si chiama il metdo loadPanelRisposte
		rispostePan = new PanelRisposte(utente, dbHandler, questionPanel, this);
		rispostePan.loadPanelRisposte();
		
		
	}
	// Il metodo setCategoriaSelected di settare la variabile categoriaSelected
	// e di filtrare le domande in base alla categoria.
	// Questo chiamando il moetodo getDomandaFiltro
	public void setCategoriaSelected(String categoriaString){
		this.categoriaSelected = categoriaString;
		dbHandler.getDomandaFiltro(categoriaSelected,new AsyncCallback<ArrayList <Domanda>>(){

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Problemi con il Server!!!");
			}

			@Override
			public void onSuccess(ArrayList<Domanda> result) {
				initListaDomande(result);
				
			}
			
		});
	}

	// Questo metodo si occupa di sistemare i Widget all'interno del RootPanel,
	// passando in input l'ArrayList delle Domande
	private void initListaDomande(ArrayList<Domanda> result) {
		if(questionPanel != null )
		{
			questionPanel.clear();
		}
		if(formPanel != null)
		{
			formPanel.clear();
		}
		HTML quest = new HTML("<h3><label class='label label-primary'>Domande: " + categoriaSelected + "</label></h3>");
		questionPanel.add(quest);
		if(result.isEmpty())
		{
			Label q=new HTML("<h3>Nessun risultato per la categoria: " + categoriaSelected + "</h3>"); 
			q.getElement().getStyle().setProperty("margin", "15px");
			questionPanel.add(q);
		}
		else
		{
			// Se l'arrayList non � vuoto viene avviato un ciclo for 
			// allo scopo di aggiungere le domande al RootPanel
			for(int i=result.size()-1;i>=0;i--){
				VerticalPanel domPanel=new VerticalPanel();
				final String testoDomanda = result.get(i).getTesto();
				Label q=new Label(result.get(i).getTesto()); 
				final int indice = result.get(i).getId();
				final String catName=result.get(i).getCategoria().getNome();
				// La label della domanda � cliccabile ed indirizza al panel delle rispste
				q.addClickHandler(new ClickHandler(){
					@Override
					public void onClick(ClickEvent event) {
						formPanel.clear();
						rispostePan.getRisposte(indice,catName, testoDomanda);
					}
					
				});
			    Label u=new Label("Inserita da: " + result.get(i).getUtente().getNomeUtente() + " data :" + result.get(i).getDataora());
			    Label cat=new Label(result.get(i).getCategoria().getNome());
			    Button deleteDomanda = new Button("Elimina");
			    domPanel.add(cat);
			    domPanel.add(q);
			    domPanel.add(u);
			    domPanel.setStyleName("panel");
			    cat.setStyleName("label label-default");
			    cat.getElement().getStyle().setProperty("margin", "0px");
			    q.setStyleName("well well-lg");
			    q.getElement().getStyle().setProperty("margin", "0px");
			    u.setStyleName("label label-primary");
			    u.getElement().getStyle().setProperty("margin", "0px");
			    deleteDomanda.setStyleName("btn btn-danger");
			    deleteDomanda.getElement().getStyle().setProperty("margin", "7px");
			    final int domanda = result.get(i).getId();
			    // Se l'utente � un amministratore sotto le domande aggiungiamo il tasto di eliminazione
			    if(tipo.equals("Amministratore") || tipo.equals("Giudice")){
			    	domPanel.add(deleteDomanda);
			    	deleteDomanda.addClickHandler(new ClickHandler(){

			    		@Override
			    		public void onClick(ClickEvent event) {
			    			// TODO Auto-generated method stub
			    			dbHandler.deleteDomanda(domanda, new AsyncCallback<Boolean>(){

								@Override
								public void onFailure(Throwable caught) {
									Window.alert("Problemi con il Server!!");
								}

								@Override
								public void onSuccess(Boolean result) {
									// TODO Auto-generated method stub
									dbHandler.getDomanda(new AsyncCallback<ArrayList<Domanda>>(){
										
										@Override
										public void onFailure(Throwable caught) {
											Window.alert("Problemi con il Server!!");
										}

										@Override
										public void onSuccess(ArrayList<Domanda> result) {
											initListaDomande(result);	
										}
									});
								}
			    				
			    			});
						
						}
			    	
			    	});
			    }
			    questionPanel.add(domPanel);
			}	
		}	
		// Se l'utente � un giudice oppure un utente semplice allora permettiamo loro di aggiungere una domanda
		if(tipo.equals("Semplice") || tipo.equals("Giudice")){
			Label labelDomanda = new Label("Domanda:");
			testoDomanda = new TextArea();
			insertDomanda = new Button("Inserisci Domanda");
			testoDomanda.setStyleName("form-control");
			formPanel.add(labelDomanda);
			labelDomanda.setStyleName("label label-info");
			insertDomanda.setStyleName("btn btn-success");
			formPanel.add(testoDomanda);
			formPanel.add(insertDomanda);
			RootPanel.get("insidePanel").add(formPanel);
			// Il tasto insertDomanda permette di aggiungere la domanda
			insertDomanda.addClickHandler(new ClickHandler(){
				@Override
				public void onClick(ClickEvent event) {
					// Verifichiamo se l'utente ha cliccato o meno una categoria
					if(categoriaSelected != null && !categoriaSelected.equals("Categorie") && !testoDomanda.getText().isEmpty() && testoDomanda.getText().length()<=300)
					{
						
						// Chiamiamo il metodo getCategorie in modo da avere l'intera root
						dbHandler.getCategorie(new AsyncCallback<Categoria>() {
							@Override
							public void onFailure(Throwable caught) {
								Window.alert("Problemi con il server!!!");
							}

							@Override
							public void onSuccess(Categoria result) {
								if(result != null)
								{
									// Assegniamo ad un oggeto Categoria il risultato del metodo ricorsivo trovaNodo
									Categoria c = GestioneCategoria.trovaNodo(result,categoriaSelected);
									// La categoria viene passata come parametro per creare l'oggeto Domanda
									Domanda dom = new Domanda(utente,c,testoDomanda.getText());
									// Chiamiamo il metodo addDomanda per aggiungere la domanda
									dbHandler.addDomanda(dom,new AsyncCallback<Boolean>(){
										@Override
										public void onFailure(Throwable caught) {
											Window.alert("Problemi con il Server!!");
										}
										@Override
										public void onSuccess(Boolean result) {
											if(result)
											{
												questionPanel.clear();
												formPanel.clear();
												loadPanelDomande();
											}
											else
											{
												Window.alert("Impossibile aggiungere la domanda!!");
											}
										}	
									});
								}
								else
								{
									Window.alert("Problemi con il Server!!");
								}
							}
						});
					}
					else
					{
						if(testoDomanda.getText().isEmpty()){
							Window.alert("Non puoi inserire una domanda senza testo");
						}else{
							if(testoDomanda.getText().length()>300){
								Window.alert("la Domanda inserita supera i 300 caratteri");
							}else{
								Window.alert("Selezionare una categoria!");
							}
						}
					}
				}	
			});
		}
	}
	
}
