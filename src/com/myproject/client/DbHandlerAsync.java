package com.myproject.client;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.myproject.shared.Categoria;
import com.myproject.shared.Domanda;
import com.myproject.shared.Giudizio;
import com.myproject.shared.Risposta;
import com.myproject.shared.Utente;


/*
 * Interfaccia sincrona del Db Handler
 * Qui venono dichiarati i metodi utili implementati sul server 
 * attraverso la classe DbHandlerImpl.java
*/
public interface DbHandlerAsync {
	
	// addUser permette di aggiungere un Utente
	void addUser(Utente userData, AsyncCallback<String> callback);

	// checkUser controlla se un utente � gia registrato al sito
	void checkUser(String[] userLogin, AsyncCallback<Utente> callback);

	//getCategorie ritorna la Categoria root con tutte le categorie e sotto categorie asscoiate
	void getCategorie(AsyncCallback<com.myproject.shared.Categoria> asyncCallback);	

	// addCategoria aggiunge una Categoria o una sottoCategoria
	void addCategoria(Categoria son, AsyncCallback<Boolean> asyncCallback);

	//removeCategoria rimuove la categoria Passata in input come parametro
	void removeCategoria(String son, AsyncCallback<Boolean> callback);

	// addDomanda aggiunge una domanda passando in input una oggeto domanda
	void addDomanda(Domanda dom, AsyncCallback<Boolean> callback);

	// getDomanda ritorna tutte le domande del sito
	void getDomanda(AsyncCallback<ArrayList<Domanda>> callback);

	// getDomandaFiltro ritorna tutte le domande per una data categoria
	void getDomandaFiltro(String categoria, AsyncCallback<ArrayList<Domanda>> callback);
	
	// deleteDomanda elimina una domanda in base all'id passato in input
	void deleteDomanda(int dom, AsyncCallback<Boolean> callback);
	
	// addRisposta aggiunge una risposta 
	void addRisposta(Risposta risp, AsyncCallback<Boolean> callback);
	
	// getRisposteFiltro ritorna tutte le risposte relative ad una domanda passando in input l'id della domanda
	void getRisposteFiltro(int domanda, AsyncCallback<ArrayList<Risposta>> callback);

	// renameCateogoria rinomina una categoria passando in input il nome della categoria 
	// da rinominare e il nuovo nome da assegnare
	void renameCategoria(String selezionata, String NuovoNome, AsyncCallback<Boolean> callback);

	// il metodo nominaGiudice permette di nominare un utente giudice passando in input il nomeUtente
	void nominaGiudice(String Utente, AsyncCallback<Boolean> callback);

	// getUtentiSemplici ritorna tutti gli utenti semplici della piattaforma
	void getUtentiSemplici(AsyncCallback<ArrayList<Utente>> callback);

	// addGiudizio aggunge un giudizio di un utente
	void addGiudizio(Giudizio giudizio, AsyncCallback<Boolean> callback);

	// pathCategoria ritorna una stringa con il path della categoria passata in input a partire dalla root
	void pathCategorie(String categoria, AsyncCallback<String> callback);

	// getGiudizi ritorna tutti i giudizi relativi ad una risposta
	void getGiudizi(int idRisposta, AsyncCallback<ArrayList<Giudizio>> callback);

	// checkGiudizio controlla se un certo utente ha gi� giudicato una risposta
	void checkGiudizio(int idRisposta, Utente utente, AsyncCallback<Boolean> callback);

	// eliminaRisposta elimina la ripsota di un utente passando in input l'id della risposta
	void eliminaRisposta(int risp, AsyncCallback<Boolean> callback);


}
