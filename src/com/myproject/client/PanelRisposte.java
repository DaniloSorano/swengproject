package com.myproject.client;

import java.util.ArrayList;

import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.myproject.shared.Giudizio;
import com.myproject.shared.Risposta;
import com.myproject.shared.Utente;

public class PanelRisposte {
	
	private String tipo;
	//private String categoriaSelected;
	private int domandaSelected;
	private String categoriaSelected;
	private DbHandlerAsync dbHandler; 
	private Utente utente;
	private TextArea testoRisposta;
	private Button tastoRisposta;
	private VerticalPanel rispPanel;
	private VerticalPanel answerPanel;
	private Label nomeCat;
	private PanelDomande panDomande;
	private String testoDomanda;
	private HorizontalPanel giudicaPanel;
	private boolean giudica;
	private boolean fatto;
	
	public PanelRisposte(Utente utente, DbHandlerAsync dbHandler, VerticalPanel answerPanel, PanelDomande panDomande) {
		this.dbHandler = dbHandler;
		this.utente = utente;
		this.answerPanel = answerPanel;
		this.panDomande = panDomande;
		//categoriaSelected = null;
		if(utente == null){
			tipo = "Guest";
		}
		else
		{
			tipo = utente.getTipo();
		}	
	}
	//carichiamo il pannello delle risposte, con il form di inserimento della risposta e la lista
	public void loadPanelRisposte() {
		answerPanel.clear();
		tastoRisposta = new Button("Inserisci Risposta");
		tastoRisposta.setStyleName("btn btn-success");
		tastoRisposta.getElement().getStyle().setProperty("marginTop","5px");
		testoRisposta = new TextArea();
		testoRisposta.setStyleName("form-control");
		//RootPanel.get("insidePanel").add(answerPanel);
		if(!tipo.equals("Amministratore") && !tipo.equals("Guest")) //se il tipo dell'utente � semplice allora potr� aggiundere una risposta
		{
			tastoRisposta.addClickHandler( new ClickHandler() {

				@Override
				public void onClick(ClickEvent event) {
					Risposta risp = new Risposta(utente, domandaSelected, testoRisposta.getText());

					if(!testoRisposta.getText().isEmpty() && testoRisposta.getText().length()<=500){
						dbHandler.addRisposta(risp, new AsyncCallback<Boolean>() {

							@Override
							public void onFailure(Throwable caught) {
								Window.alert("Problemi con il server!!!");
							}

							@Override
							public void onSuccess(Boolean result) {
								if(result)
								{
									getRisposte(domandaSelected,categoriaSelected, testoDomanda);//metodo per la lista delle risposte
								}
							}
							
						});
					}
					else if(testoRisposta.getText().isEmpty())
					{
						Window.alert("Inserire una risposta prima di procedere");
					}
					else
					{
						Window.alert("La risposta supera i 500 caratteri!");
					}
						}
						
					});
				}
	}
	
	//questo metodo ci permette di ottenere la lista delle risposte in base alla domanda cliccata, ricaricato a runtime nel loadPanel
		public void getRisposte(int domandaInt,String categoria, String testo){
			this.domandaSelected = domandaInt;
			this.categoriaSelected=categoria;
			this.testoDomanda = testo;
			dbHandler.getRisposteFiltro(domandaSelected,new AsyncCallback<ArrayList <Risposta>>(){
				
				@Override
				public void onFailure(Throwable caught) {
					Window.alert("Problemi con il server!!!");
				}
				
				@Override
				public void onSuccess(ArrayList<Risposta> result) {
					initListaRisposte(result);
				}		
			});
		}
//		questo metodo viene ricaricato ogniqualvolta si aggiunge una nuova risposta, visualizzando cos� la lista con la nuova risposta
		private void initListaRisposte(ArrayList<Risposta> result){
			final ArrayList<Risposta> listaRisposte = result;
			final Label labelDomanda = new HTML("<h4><label class='label label-primary'>Domanda</label></h4");
			final Label dom = new Label(testoDomanda);
			dom.setStyleName("well well-lg");
			final Label labelRisposte = new HTML("<h4><label class='label label-success'>Risposte</label></h4>");
			final Label scriviRisposta = new Label("Scrivi Risposta: ");
			scriviRisposta.getElement().getStyle().setProperty("marginTop", "30px");
			final Button indietro = new Button("Indietro");
			indietro.getElement().getStyle().setProperty("marginTop", "20px");
			indietro.setStyleName("btn btn-warning");
			indietro.addClickHandler(new ClickHandler(){

				@Override
				public void onClick(ClickEvent event) {
					answerPanel.clear();
					panDomande.loadPanelDomande();
					
				}
				
			});
			if(answerPanel != null)
			{
				answerPanel.clear();
			}
			dbHandler.pathCategorie(categoriaSelected,new AsyncCallback<String>(){

				@Override
				public void onFailure(Throwable caught) {
					Window.alert("Problemi con il server!!!");
				}

				@Override //inizializzazione del form per rispondere
				public void onSuccess(String result) {
					nomeCat=new HTML("<h3><label class='label label-default'>Categoria: " + result + "</label></h3>");
					nomeCat.getElement().getStyle().setProperty("marginTop", "30px");
					nomeCat.getElement().getStyle().setProperty("marginBottom", "30px");
					if(listaRisposte.isEmpty())
					{
						//labelDomanda = new HTML("<h4><label class='label label-primary'>Domanda</label></h4");
						Label question=new HTML("<h3>Nessuna risposta</h3>"); 
						//Label labelRisposte = new HTML("<h4><label class='label label-success'>Risposte</label></h4>");
						answerPanel.add(nomeCat);
						answerPanel.add(labelDomanda);
						answerPanel.add(dom);
						answerPanel.add(labelRisposte);
						answerPanel.add(question);
						if(!tipo.equals("Amministratore") && !tipo.equals("Guest"))
						{
							answerPanel.add(scriviRisposta);
							answerPanel.add(testoRisposta);
							answerPanel.add(tastoRisposta);
						}
						answerPanel.add(indietro);
						//indietro
					}
					else
					{
					
						// Inizializzo l panello delle risposte
						rispPanel = new VerticalPanel();
						//Label labelDomanda = new HTML("<h4><label class='label label-primary'>Domanda</label></h5>");
						nomeCat=new HTML("<h3><label class='label label-default'>Categoria: " + result + "</label></h3>");
						nomeCat.getElement().getStyle().setProperty("marginTop", "30px");
						nomeCat.getElement().getStyle().setProperty("marginBottom", "30px");
						rispPanel.add(nomeCat);
						rispPanel.add(labelDomanda);
						rispPanel.add(dom);
						//Label labelRisposte = new HTML("<h4><label class='label label-success'>Risposte</label></h5>");
						rispPanel.add(labelRisposte);
				
						
						for(int i = 0; i < listaRisposte.size(); i++){
			
							final int indice = i;
							final Label q=new Label(listaRisposte.get(indice).getTesto()); 
							final Label u=new Label("Inserita da: " + listaRisposte.get(indice).getUtente().getNomeUtente() + " data :" + listaRisposte.get(indice).getDataora());
							//final Label m=new Label("Media giudizi :"+listaRisposte.get(indice).getMedia());
							final Button eliminaRisp = new Button("Elimina Risposta");
							eliminaRisp.setStyleName("btn btn-danger");
							final int idRisposta = listaRisposte.get(indice).getId();
							    
							q.setStyleName("well well-sm");
							q.getElement().getStyle().setProperty("margin", "0px");
							q.getElement().getStyle().setProperty("marginTop", "30px");
							u.setStyleName("label label-success");
							u.getElement().getStyle().setProperty("margin", "0px");
				
							if(!tipo.equals("Guest"))
							{
					
								
									dbHandler.checkGiudizio(idRisposta, utente, new AsyncCallback<Boolean>(){
									
										@Override
										public void onFailure(Throwable caught) {
											Window.alert("Problemi con il server!!!");
										}
			
										@Override
										public void onSuccess(Boolean result) {
											//panelRisp.add(m);
		
											eliminaRisp.addClickHandler( new ClickHandler() {
			
												@Override
												public void onClick(ClickEvent event) {
													dbHandler.eliminaRisposta(idRisposta, new AsyncCallback<Boolean>() {
			
												@Override
												public void onFailure(Throwable caught) {
													Window.alert("Problemi con il server!!!");
																
												}
			
												@Override
												public void onSuccess(Boolean result) {
																	
													getRisposte(domandaSelected, categoriaSelected,testoDomanda);
																	
												}
																
											});				
										}		 
									});
										//inizializzazione del pannello del giudizio, nel caso in cui il tipo di utente sia Giudice
										giudica = false;
										if(result == false && tipo.equals("Giudice")){
											giudicaPanel =new HorizontalPanel();
											giudicaPanel.getElement().getStyle().setProperty("marginTop", "10px");
											giudicaPanel.getElement().getStyle().setProperty("marginBottom", "10px");
											Button tastoGiudica =new Button("Giudica");
											tastoGiudica.setStyleName("btn btn-success");
											String votoString="VotoRadioButton"+indice;
											final RadioButton voto0 = new RadioButton(votoString,"0");
											voto0.setValue(true);
											final RadioButton voto1 = new RadioButton(votoString,"1");
											final RadioButton voto2 = new RadioButton(votoString,"2");
											final RadioButton voto3 = new RadioButton(votoString,"3");
											final RadioButton voto4 = new RadioButton(votoString,"4");
											final RadioButton voto5 = new RadioButton(votoString,"5");
											giudicaPanel.add(voto0);
											giudicaPanel.add(voto1);
											giudicaPanel.add(voto2); 
											giudicaPanel.add(voto3);
											giudicaPanel.add(voto4);
											giudicaPanel.add(voto5);
											giudicaPanel.add(tastoGiudica);
											tastoGiudica.getElement().getStyle().setProperty("marginLeft", "10px");
											giudica = true;
											tastoGiudica.addClickHandler(new ClickHandler() {
												@Override
												public void onClick(ClickEvent event) {
													//giudicaPanel.clear();
													int voto = 0;
													if(voto0.getValue())
													{
														voto = 0;
													}
													if(voto1.getValue())
													{
														voto = 1;
													}
													if(voto2.getValue())
													{
														voto = 2;
													}
													if(voto3.getValue())
													{
														voto = 3;
													}
													if(voto4.getValue())
													{
														voto = 4;
													}
													if(voto5.getValue())
													{
														voto = 5;
													}
													//metodo per aggiungere un giudizio			
													Giudizio giudizio = new Giudizio(voto, idRisposta, utente);
													dbHandler.addGiudizio(giudizio, new AsyncCallback<Boolean>() {
		
														@Override
														public synchronized void onFailure(Throwable caught) {
															Window.alert("Problemi con il server!!!");
														}
		
														@Override
														public synchronized void onSuccess(Boolean result) {
															//giudicaPanel.clear();
															getRisposte(domandaSelected, categoriaSelected, testoDomanda);
															//giudicaPanel.clear();
														}
											
													});
										
												}
													    	
											});
												    
										}
									
									}
								 	
								});
									  
							}
							//metodo per visualizzare tutti i giudizi in base alla risposta cliccata
							dbHandler.getGiudizi(idRisposta, new AsyncCallback<ArrayList<Giudizio>>(){
	
								@Override
								public synchronized void onFailure(Throwable caught) {
										Window.alert("nessun giudizio per la risposta");
								}
	
								@Override
								public synchronized void onSuccess(ArrayList<Giudizio> result) {
									rispPanel.add(q);
									rispPanel.add(u);
									if(giudica)
									{
										rispPanel.add(giudicaPanel);
									}
									if(tipo.equals("Amministratore") || tipo.equals("Giudice"))
									{
										rispPanel.add(eliminaRisp);
									}
									for(int i = 0; i<result.size(); i++)
									{
										Label label = new Label("VOTO: " + String.valueOf(result.get(i).getVoto()) + "  UTENTE: " + String.valueOf(result.get(i).getUtente().getNomeUtente()));
										label.setStyleName("label label-info");
										rispPanel.add(label);
									}
									fatto = false;
								}
									
							});

							answerPanel.add(rispPanel);
							if(!tipo.equals("Amministratore") && !tipo.equals("Guest"))
							{
								answerPanel.add(scriviRisposta);
								answerPanel.add(testoRisposta);
								answerPanel.add(tastoRisposta);
							}
							answerPanel.add(indietro);		
							
						}
					}	
				}
			});
		}
			

	}


	
