package com.myproject.client;

import java.util.ArrayList;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Tree;
import com.google.gwt.user.client.ui.TreeItem;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.myproject.shared.Utente;

public class PanelNominaGiudici {
	
	private DbHandlerAsync dbHandler;
	private VerticalPanel vertPan;
	private Tree treeUtenti;
	private Button tastoNominaGiudici;
	private Button nomina;
	
	// Il costruttore inizializza solo la variabile di istanza dbHandler 
	public PanelNominaGiudici(DbHandlerAsync dbHandler) {
		this.dbHandler = dbHandler;
	}
	
	/*
	 * Questo metodo come gli altri si occupa dell'inizializzazione dei Widget
	 */
	public void loadPanelNominaGiudici() {
		vertPan = new VerticalPanel();
		tastoNominaGiudici = new Button("Nomina Giudice");
		tastoNominaGiudici.setStyleName("btn btn-primary");
		treeUtenti = new Tree();
		nomina = new Button("Nomina");
		nomina.setStyleName("btn btn-success");
		// Al click del tasto nomina Giudice viene visualizzato l'oggetto Tree degli utenti
		tastoNominaGiudici.addClickHandler( new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
	
				
				treeUtenti.clear();
				vertPan.remove(nomina);
				// La chiamata al server getUtentiSemplici permette di ottenere 
				// tutti gli utenti semplice della piattaforma
				dbHandler.getUtentiSemplici(new AsyncCallback<ArrayList<Utente>>() {

					@Override
					public void onFailure(Throwable caught) {
						Window.alert("Problemi con il Server!!");
					}

					@Override
					public void onSuccess(ArrayList<Utente> result) {
						if(result != null)
						{
							treeUtenti.addItem(createTreeUtenti(result));
						}
						else
						{
							Window.alert("Non ci sono Utenti semplici!!");
						}
					}
					
				});
				
				vertPan.add(treeUtenti);
				nomina = new Button("Nomina");
				nomina.setStyleName("btn btn-success");
				vertPan.add(nomina);
				nomina.setVisible(false);
			}
			
		});
		vertPan.add(tastoNominaGiudici);
		treeUtenti = new Tree();
		nomina.setVisible(false);
		RootPanel.get("rightPanel").add(vertPan);
		treeSelectionHandler();
	}
	
	// Questo metodo si occupa di creare l'albero deli Utenti semplici
	private TreeItem createTreeUtenti(ArrayList<Utente> utentiSemplici) {
		TreeItem treeItem = new TreeItem();
		treeItem.setText("Utenti Semplici");
		for(int i = 0; i < utentiSemplici.size(); i++)
		{
			treeItem.addTextItem(utentiSemplici.get(i).getNomeUtente());
		}
		return treeItem;
	}
	// Questo metodo gestisce la selezione degli elementi dell'albero
	private void treeSelectionHandler() {
		// Se viene selezionato un elemento dell'albero degli utenti viene visualizzato il tasto nomina
		treeUtenti.addSelectionHandler( new SelectionHandler<TreeItem>() {

			@Override
			public void onSelection(SelectionEvent<TreeItem> event) {
				final String username = event.getSelectedItem().getText();
				if(!username.equals("Utenti Semplici"))
				{
					if (nomina.isVisible()) {
						nomina.setVisible(false);
					}
					else
					{
						nomina.setVisible(true);
						// Cliccando il tasto nomina l'utente viene nominato giudice
						nomina.addClickHandler(new ClickHandler() {

							@Override
							public void onClick(ClickEvent event) {
								// Facciamo una chiamata al Server attraverso il metodo nominaGiudice
								dbHandler.nominaGiudice(username, new AsyncCallback<Boolean>(){

									@Override
									public void onFailure(Throwable caught) {
										Window.alert("Problemi con il Server!!");
									}

									@Override
									public void onSuccess(Boolean result) {
										if(result)
										{
											vertPan.clear();
											loadPanelNominaGiudici();
										}
										else
										{
											
										}
									}
									
								});
							}
							
						});
					}
				}
				
				
			}
			
		});
	}
}
