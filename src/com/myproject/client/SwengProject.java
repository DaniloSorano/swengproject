package com.myproject.client;



import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;

public class SwengProject implements EntryPoint {
	
	//Creiamo ed istanziamo l'oggetto DbHandlerAsync q
	private final DbHandlerAsync dbHandler = GWT.create(DbHandler.class);

	public void onModuleLoad() {
			Login login = new Login(dbHandler);
			login.loadLoginPanel();
		}
	
	}

