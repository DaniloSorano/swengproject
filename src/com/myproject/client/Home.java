package com.myproject.client;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Tree;
import com.myproject.shared.Utente;

/*
 * La classe Home si occupa di caricare il panel dell'albero delle categorie(PanelCategorie), 
 * creare il tasto di Log-Out e il panel per nominare i giudici(PanelNominaGiudici)
 */
public class Home {
	private Utente utente;
	private DbHandlerAsync dbHandler;
	private Tree tree;
	private Login login;
	
	/* 
	 * Il costruttore della classe Home contiene l'oggetto dbHandler creato nella classe Login, 
	 * l'oggetto utente relativo all'utente che ha fatto il login ed
	 * uno oggetto Login per permettere all'utente di poter fare il logOut 
	 * e tornare alla pagina precedente
	*/
	public Home(Utente utente, DbHandlerAsync dbHandler, Login login) {
		this.utente = utente;
		this.dbHandler = dbHandler;
		// Qui inizializziamo l'oggetto Tree utilizzato per visualizzare le categorie
		tree = new Tree();
		this.login = login;
	}
	
	/*
	 * Il metodo loadHomePanel si occupa di creare gli oggetti delle classi
	 * PanelCategorie e PanelNominaGiudici.
	 * Inoltre crea il tasto di logout e ne gestisce l'evento di click
	 */
	public void loadHomePanel(){
		
		// Creazione del PanelCategorie
		PanelCategorie categoriePan = new PanelCategorie(utente, dbHandler, tree);
		categoriePan.loadPanelCategorie();
		// Controlliamo che l'utente si un Amministratore
		if(utente != null)
		{
			if(utente.getTipo().equals("Amministratore"))
			{
				// Se � un amministratore creiamo l'oggetto PanelNominaGiudici
				PanelNominaGiudici giudiciPan = new PanelNominaGiudici(dbHandler);
				// lanciamo il metodo per iniziallizare il Panel
				giudiciPan.loadPanelNominaGiudici();
			}
		}
		//Creiamo il Button logout
		Button logout = new Button();
		if(utente == null) {
			logout.setText("Accedi");
		}
		else
		{
			logout.setText("Log-Out");
		}
		logout.setStyleName("btn btn-default");
		RootPanel.get("rightPanel").add(logout);
		// Gestiamo l'evento di click del tasto logout
		logout.addClickHandler( new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				// Puliamo il i panel e carichiamo il metodo loadLoginPanel
				RootPanel.get("rightPanel").clear();
				RootPanel.get("leftPanel").clear();
				RootPanel.get("insidePanel").clear();
				login.loadLoginPanel();
			}
			
		});
	}
}
