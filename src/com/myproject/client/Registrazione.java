package com.myproject.client;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.datepicker.client.DateBox;
import com.myproject.shared.FieldVerifier;
import com.myproject.shared.Utente;

public class Registrazione {
	
	private DbHandlerAsync dbHandler;
	
	public Registrazione(DbHandlerAsync dbHandler) {
		this.dbHandler = dbHandler;
	}
	
	public void loadRegistrazionePanel() {
		//Il pannello viene ricaricato in questo modo ogniqualvolta vengono fatte delle operazioni
		final VerticalPanel panel = new VerticalPanel();
		final Button sendButton = new Button("Vai");
		sendButton.setStyleName("btn btn-success col-md-offset-6");
		final TextBox nameField = new TextBox();
		nameField.setStyleName("form-control text-center");
		final TextBox surnameField = new TextBox();
		surnameField.setStyleName("form-control text-center");
		final TextBox usernameField =new TextBox();
		usernameField.setStyleName("form-control text-center");
		final TextBox luogoNascita =new TextBox();
		luogoNascita.setStyleName("form-control text-center");
		final TextBox luogoResidenza =new TextBox();
		luogoResidenza.setStyleName("form-control text-center");
		final TextBox emailField=new TextBox();
		emailField.setStyleName("form-control text-center");
		final PasswordTextBox pssField=new PasswordTextBox();
		pssField.setStyleName("form-control text-center");
		final PasswordTextBox repssField=new PasswordTextBox();
		repssField.setStyleName("form-control text-center");
		final RadioButton rbm = new RadioButton("GenderRadioGroup", "M");
		final RadioButton rbf = new RadioButton("GenderRadioGroup", "F");
		DateTimeFormat dateFormat = DateTimeFormat.getLongDateFormat();
		final DateBox dataNascita=new DateBox();
		dataNascita.setFormat(new DateBox.DefaultFormat(dateFormat));
		dataNascita.setStyleName("form-control text-center");
		final Button login = new Button("login");
		login.setStyleName("btn btn-primary col-md-offset-12");
		
		// We can add style names to widget
		sendButton.addStyleName("sendButton");
		panel.add(new HTML("<label>Email(Obbligatorio):</label>"));
		panel.add(emailField);
		panel.add(new HTML("<label>Username(Obbligatorio):</label>"));
		panel.add(usernameField);
		panel.add(new HTML("<label>Password(Obbligatorio):</label>"));
		panel.add(pssField);
		panel.add(new HTML("<label>Ripeti Password(Obbligatorio):</label>"));
		panel.add(repssField);
		panel.add(new HTML("<label>Nome :</label>"));
		panel.add(nameField);
		panel.add(new HTML("<label>Cognome :</label>"));
		panel.add(surnameField);
		panel.add(new HTML("<label>Sesso :</label>"));
		rbm.setValue(true);
		panel.add(rbm);
		panel.add(rbf);
		panel.add(new HTML("<label>Luogo di Residenza :</label>"));
		panel.add(luogoResidenza);
		panel.add(new HTML("<label>Data di Nascita :</label>"));
		panel.add(dataNascita);
		panel.add(new HTML("<label>Luogo di Nascita :</label>"));
		panel.add(luogoNascita);
		HorizontalPanel horpan = new HorizontalPanel();
		horpan.getElement().getStyle().setProperty("marginTop","10px");
		horpan.add(sendButton);
	    login.addStyleName("loginButton");
	    horpan.add(login);
	    panel.add(horpan);
	    
	    RootPanel.get("insidePanel").add(panel);
	    //cliccando sul bottone login viene inizializzato il panel specifico
		login.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event){
				RootPanel.get("insidePanel").clear();
				Login login = new Login(dbHandler);
				login.loadLoginPanel();
			}
		});
		//click sul bottone di registrazione e controlli dei campi
		sendButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event){
				boolean verifier=true;
				String gender = "";
				String error= "";
				String data;
				data="";
				if(rbm.getValue())
				{
					gender = rbm.getText();
				}
				else
				{
					gender = rbf.getText();
				}
				if (!FieldVerifier.isValidField(usernameField.getText())){
					verifier=false;
					error="Inserire un campo username di almeno 4 caratteri";
				}
				if (!FieldVerifier.isValidField(emailField.getText())){
					verifier=false;
					error=error + "\n Inserire un campo email di almeno 4 caratteri";
				}
				if (!FieldVerifier.passwordLenght(pssField.getText())){
					verifier=false;
					error=error + "\n Inserire una password di almeno 8 caratteri";
				}
				if(!FieldVerifier.isValidPassword(pssField.getText(),repssField.getText()))
				{
					verifier=false;
					error=error + "\n Le passoword non coincidono";
				}
				if(dataNascita.getValue()!=null){
					data=dataNascita.getValue().toString();
				}
				if(verifier) {
				Utente userLogin = new Utente(usernameField.getText(),nameField.getText(),surnameField.getText(), 
						data, gender,luogoResidenza.getText(),luogoNascita.getText(), 
						emailField.getText(), pssField.getText());
				
				dbHandler.addUser(userLogin, new AsyncCallback<String>() {
					
					public void onFailure(Throwable caught) {
						Window.alert("Problemi con il server!!!");
					}

					public void onSuccess(String result) {
						if(result.equals("Success"))
						{
							RootPanel.get("insidePanel").clear();
							Login login = new Login(dbHandler);
							login.loadLoginPanel();
						}
						else
						{
							Window.alert("Utente gia registrato!");
						}
					}
				});
			}else{
				Window.alert(error);
				error="";
			}
		}
	});	
  }
}
