package com.myproject.shared;

import java.util.ArrayList;
/* classe Categoria utilizzata nel sistema contentente anche i metodi per aggiungere una sottocategoria ed eliminarla*/
public class Categoria implements java.io.Serializable {
	private Categoria padre;
	private String nome;
	private ArrayList<Categoria> sottoCategoria;
	public Categoria() {
		
	}
	public Categoria(String nome) {
		this.nome = nome;
		this.sottoCategoria = null;
		this.padre = null;
	}
	public Categoria getPadre() {
		return padre;
	}
	public void setPadre(Categoria padre) {
		this.padre = padre;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public ArrayList<Categoria> getSottoCategoria() {
		return sottoCategoria;
	}
	public void addSottoCategoria(Categoria categoria) {
		if(this.sottoCategoria == null)
		{
			this.sottoCategoria = new ArrayList<Categoria>();
		}
		categoria.setPadre(this);
		this.sottoCategoria.add(categoria);
	}
	public boolean removeSottoCategoria(String categoria) {
		if(this.sottoCategoria != null)
		{
			for(int i = 0; i < sottoCategoria.size(); i++)
			{
				if(sottoCategoria.get(i).getNome().equals(categoria))
				{
					if(i==0)
					{
						this.sottoCategoria = null;
					}
					else
					{
						this.sottoCategoria.remove(i);
					}
					return true;
				}
			}
			return false;
		}
		else
		{
			return false;
		}
	}
}
