package com.myproject.shared;

/*
 FieldVerifier controlla se i campi dell'email e dell'utente sono validi
 la classi in Shared vengono utilizzate sia dal Client che dal Server
 */
public class FieldVerifier {

	public static boolean isValid(String s){
		if (s==null){
			return false;
		}
		return true;
	}
	public static boolean isValidField(String name) {
		if (name == null) {
			return false;
		}
		return name.length() > 4;
	}
	public static boolean isValidPassword(String pss,String repss){
		if (pss!=repss){
			return false;
		}
		return true;
	}
	public static boolean passwordLenght(String pss){
		if(pss.length()<8){
			return false;
		}else{
			return true;
		}
	}
}
