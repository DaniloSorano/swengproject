package com.myproject.shared;

import java.util.Date;
//CLasse che descrive una Rispota contenente anche due metodi sulla media dei giudizi
public class Risposta implements java.io.Serializable {
	private Utente utente;
	private String testo,dataora;
	private int idDomanda;
	private int id;
	private double media;
	public Risposta(){
		
	}
	public Risposta (Utente utente,int domanda,String testo){
		Date data=new Date();
		String d=data.toString();
		String[] t=d.split(" ");
		dataora=t[2]+" "+t[1]+" "+t[t.length-1]+" "+t[3];
		this.utente=utente;
		this.idDomanda=domanda;
		this.testo=testo;
		this.id = 0;
		this.media = -1;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public Utente getUtente() {
		return utente;
	}
	public void setUtente(Utente utente) {
		this.utente = utente;
	}
	public String getTesto() {
		return testo;
	}
	public void setTesto(String testo) {
		this.testo = testo;
	}
	public String getDataora() {
		return dataora;
	}
	/*public void setDataora(String dataora) {
		this.dataora = dataora;
	}*/
	public int getIdDomanda() {
		return idDomanda;
	}
	public void setIdDomanda(int id) {
		this.idDomanda = id;
	}
	public double getMedia() {
		return media;
	}
	public void setMedia(double media) {
		this.media = media;
	}
}
