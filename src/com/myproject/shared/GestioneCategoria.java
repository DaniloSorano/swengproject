package com.myproject.shared;

import java.util.ArrayList;
/* la classe Gestione Categoria � di fondamentale importanza, trovandosi in Shared pu� essere utilizzata
 sia dal client che dal server, e contiene tutti i metodi che lavorano sulle categorie utilizzate dal sistema*/

public class GestioneCategoria {
	//con questo metodo troviamo la categoria che ci serve
	public static int trovaCategoria(Categoria r,String c){
		Categoria temp;
		int trovato,i;
		if(r==null){
			return 0;
		}
		if(r.getNome().equalsIgnoreCase(c)){ //se l'oggetto contentente � uguale a quello da ricercare (stringa)
			return 1;
		}
		trovato=0;
		i=0;
		ArrayList<Categoria> sottoCat=r.getSottoCategoria();
		if(sottoCat==null){
			return 0;
		}
		//altrimenti, una volta ottenuta una lista delle sottocategorie, la scorriamo RICORSIVAMENTE finch� non troviamo quella voluta
		temp=sottoCat.get(i);
		while(i<sottoCat.size() && trovato!=1){
			if(trovaCategoria(temp,c)==1){
				trovato=1;
				break;
			}
			i++;
			if(i>=sottoCat.size())
			{
				return 0;
			}
			temp=sottoCat.get(i);
		}
		return trovato;
	}
	//rimuoviamo una categoria specifica
	public static int removeCategoriaHandler(Categoria r, String c){
		Categoria temp;
		int trovato,i;

		if(r==null){
			return 0;
		}
		if(r.getNome().equalsIgnoreCase(c)){ //come prima se il nome corrisponde a quella cliccata (root), prendiamo il padre e rimuoviamo
			//tutte le sottocategorie
			r.getPadre().removeSottoCategoria(c);
			return 1;
		}
		trovato=0;
		i=0;
		ArrayList<Categoria> sottoCat=r.getSottoCategoria();
		if(sottoCat==null){
			return 0;
		}
		//altrimenti ricorsivamente scorriamo la lista delle sottocategorie per poi eliminare quella specifica e tutte quelle sottostanti
		temp=sottoCat.get(i);
		while(i<sottoCat.size() && trovato!=1){
			if(removeCategoriaHandler(temp,c)==1){
				
				trovato=1;
				break;
			}
			i++;
			if(i>=sottoCat.size())
			{
				return 0;
			}
			temp=sottoCat.get(i);
		}
		return trovato;
	}
	//metodo per restituire il nodo che ci interessa passando la categoria root e il nome della categoria da trovare
	public static Categoria trovaNodo(Categoria r,String c){
		Categoria temp,cat;
		cat=null;
		int trovato,i;
		if(r==null){
			cat=null;
			return cat;
		}
		if(r.getNome().equalsIgnoreCase(c)){
			cat=r;
			return cat;
		}
		trovato=0;
		i=0;
		ArrayList<Categoria> sottoCat=r.getSottoCategoria();
		if(sottoCat==null){
			cat=null;
			return cat;
		}
		temp=sottoCat.get(i);
		while(i<sottoCat.size() && trovato!=1){
			Categoria category = trovaNodo(temp,c);
			if(category!=null){
				trovato=1;
				cat=category;
				//return cat;
				break;
			}
			i++;
			if(i>=sottoCat.size())
			{
				cat=null;
				return cat;
			}
			temp=sottoCat.get(i);
			//i++;
		}
		return cat;
	}
	//metodo per controllare se il nome della cateogiria cliccata corrisponde a quella passata
	public static boolean checkCategoria(Categoria f,Categoria r){
		while(!f.getNome().equals("Categorie")){
			if(f.getNome().equals(r.getNome())){
				return true;
			}else{
				f=f.getPadre();
			}
		}
		return false;
	}
	//una volta controllato se il nome corisponde alla categoria specifica, ne stampiamo la strittura ad albero
	public static String stampaCategorie(Categoria f,Categoria r){
		String s=f.getNome();
		while(!f.getNome().equals("Categorie")){
			if(f.getNome().equals(r.getNome())){
				return s;
			}else{
				f=f.getPadre();
				if(!f.getNome().equals(r.getNome())){
	            s=f.getNome()+" > "+s;
				}
			}
		}
		return s;
	}
//metodo per rinominare una cateogira funzionante come il removeCategoria
public static int renameCategoriaHandler(Categoria r, String c, String nuovoNome){
	Categoria temp;
	int trovato,i;

	if(r==null){
		return 0;
	}
	if(r.getNome().equalsIgnoreCase(c)){
		r.setNome(nuovoNome);
		return 1;
	}
	trovato=0;
	i=0;
	ArrayList<Categoria> sottoCat=r.getSottoCategoria();
	if(sottoCat==null){
		return 0;
	}
	temp=sottoCat.get(i);
	while(i<sottoCat.size() && trovato!=1){
		if(renameCategoriaHandler(temp,c,nuovoNome)==1){
			
			trovato=1;
			break;
		}
		i++;
		if(i>=sottoCat.size())
		{
			return 0;
		}
		temp=sottoCat.get(i);
	}
	return trovato;
}
}
