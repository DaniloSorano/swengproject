package com.myproject.shared;

//CLASSE UTENTE
//Per quanto riguarda il tipo abbiamo voluto implementare 3 metodi per ogni tipo utente per facilit� di implementazione
//siccome il TIPIUTENTE � un array di 3 stringhe "Semplice", "Amministratore" e "Giudice"
public class Utente implements java.io.Serializable {

	private static final long serialVersionUID = 6920472853042767550L;
	private String nome;
	private String cognome;
	private String dataNascita;
	private String sesso;
	private String residenza;
	private String luogoNascita;
	private String email;
	private String password;
	private String nomeUtente;
	private String tipoUtente;
	private final String[] TIPIUTENTE = new String[] {"Semplice", "Amministratore", "Giudice"};
	
	public Utente() {
		
	}
	
	public Utente(String nomeUtente, String nome, String cognome, String dataNascita, 
			String sesso, String residenza, String luogoNascita, 
			String email, String password) {
		this.nomeUtente= nomeUtente;
		this.nome = nome;
		this.cognome = cognome;
		this.dataNascita = dataNascita;
		this.sesso = sesso;
		this.residenza = residenza;
		this.luogoNascita = luogoNascita;
		this.email = email;
		this.password = password;
		this.tipoUtente = TIPIUTENTE[0];
	}
	public Utente(String nomeUtente,String password){
		this.nomeUtente=nomeUtente;
		this.password=password;
		this.tipoUtente= TIPIUTENTE[1];
	}
	public String getNomeUtente() {
		return nomeUtente;
	}
	public void setNomeUtente(String nomeUtente) {
		this.nomeUtente = nomeUtente;
	}
	public String getTipo() {
		return this.tipoUtente;
	}
	public void setTipoSemplice() { 
		this.tipoUtente = TIPIUTENTE[0];
	}
	public void setTipoAmministratore() {
		this.tipoUtente = TIPIUTENTE[1];
	}
	public void setTipoGiudice() {
		this.tipoUtente = TIPIUTENTE[2];
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCognome() {
		return cognome;
	}
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	public String getDataNascita() {
		return dataNascita;
	}
	public void setDataNascita(String dataNascita) {
		this.dataNascita = dataNascita;
	}
	public String getSesso() {
		return sesso;
	}
	public void setSesso(String sesso) {
		this.sesso = sesso;
	}
	public String getResidenza() {
		return residenza;
	}
	public void setResidenza(String residenza) {
		this.residenza = residenza;
	}
	public String getLuogoNascita() {
		return luogoNascita;
	}
	public void setLuogoNascita(String luogoNascita) {
		this.luogoNascita = luogoNascita;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
}

