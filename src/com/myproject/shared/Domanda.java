package com.myproject.shared;


import java.util.Date;
//Classe Domanda utilizzata nel progetto che implementa Serializable

public class Domanda  implements java.io.Serializable{
	private Utente utente;
	private Categoria categoria;
	private String testo,dataora;
	private int id;
	public Domanda(){
		
	}
	public Domanda (Utente utente,Categoria categoria,String testo){
		Date data=new Date();
		String d=data.toString();
		String[] t=d.split(" ");
		dataora=t[2]+" "+t[1]+" "+t[t.length-1]+" "+t[3];
		this.utente=utente;
		this.categoria=categoria;
		this.testo=testo;
		this.id=0;
	}

	public Utente getUtente() {
		return utente;
	}

	public void setUtente(Utente utente) {
		this.utente = utente;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	public String getTesto() {
		return testo;
	}

	public void setTesto(String testo) {
		this.testo = testo;
	}
	public String getDataora() {
		return dataora;
	}
	public void setId(int id){
		this.id=id;
	}
	public int getId(){
		return id;
	}
}
