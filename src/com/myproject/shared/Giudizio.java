package com.myproject.shared;
//Classe per implementare il Giudizio contentente tutti i metodo e attributi utilizzati
public class Giudizio  implements java.io.Serializable{
	int idRisposta,voto;
	Utente utente;
	public Giudizio(){
		
	}
	public Giudizio(int voto,int id,Utente utente){
		this.voto=voto;
		this.idRisposta=id;
		this.utente=utente;
	}
	public int getId(){
		return idRisposta;
	}
	public void setId(int id){
		this.idRisposta=id;
	}
	public int getVoto(){
		return voto;
	}
	public void setVoto(int voto){
		this.voto=voto;
	}
	public Utente getUtente(){
		return utente;
	}
	public void setUtente(Utente utente){
		this.utente=utente;
	}
}
